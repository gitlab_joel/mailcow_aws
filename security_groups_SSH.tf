resource "aws_security_group" "SSH_SG" {
  name = "SSH_SG"
  description = "Allow inbound traffic for SSH Connections"

  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }
}
